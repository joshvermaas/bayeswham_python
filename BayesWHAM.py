#This code is largely unaltered from the original BayesWHAM implementation, other than in its formatting.
#The section that has changed to reflect Gibbs sampling of a Dirichlet prior is marked by **JVV**
"""

% Copyright:	Andrew L. Ferguson, UIUC 
% Last updated:	2 Jan 2016

% SYNOPSIS
%
% code to compute dim-dimensional free energy surface from umbrella simulations by maximization of Bayes posterior, where posterior = likelihood * prior / evidence ~ likelihood * pior, plus estimate uncertainties by Metropolis-Hastings (MH) sampling from posterior  
%
% 1. assumes 	(i)   harmonic restraining potentials in dim-dimensional umbrella variables psi 
%            	(ii)  all simulations conducted at same temperature, T
%            	(iii) rectilinear binning of histograms collected over each umbrella simulation  
% 2. computes maximum a posteriori (MAP) estimate of unbiased probability distribution p(psi) by maximizing P(theta|data) = P(data|theta)*P(theta)/P(data), which is equivalent to solving WHAM equation with a Bayesian prior; 
%    with no prior (i.e., uniform prior) this is the maximum likelihood estimate, which is precisely equivalent to solving the WHAM equations  
% 3. estimates uncertainties by sampling from the posterior distribution P(theta|data) using Metropolis-Hastings algorithm 

% REFERENCES
%
% WHAM:				Kumar, S. et al., J. Comput. Chem. 13 8 1011-1021 (1992) 
%					Kumar, S. et al., J. Comput. Chem. 16 11 1339-1350 (1995) 
%					Ferrenberg, A.M. & Swendsen, R.H., Phys. Rev. Lett. 63 1195-1198 (1989) 
%					Roux, B., Comput. Phys. Comm. 91 275-282 (1995) 
% Bayes sampling:	Gallichio, E. et al., J. Phys. Chem. B 109 6722-6731 (2005)	
%					Bartels, C., Chem. Phys. Lett. 331 446-454 (2000) 
%					Zhu, F. & Hummer, G., J. Comput. Chem. 33 4 453-465 (2012) 
%					Habeck, M., Phys. Rev. Lett. 109 100601 (2012) 

% INPUTS
%
% dim                       - [int] dimensionality of umbrella sampling data in psi(1:dim) = number of coordinates in which umbrella sampling was conducted 
% periodicity               - [1 x dim bool] periodicity in each dimension across range of histogram bins 
%                             -> periodicity(i) == 0 => no periodicity in dimension i; periodicity(i) == 1 => periodicity in dimension i with period defined by range of histogram bins 
% T                         - [float] temperature in Kelvin at which all umbrella sampling simulations were conducted  
% harmonicBiasesFile        - [str] path to text file containing (1 + 2*dim) columns and S rows listing location and strength of biasing potentials in each of the S umbrella simulations 
%                             -> col 1 = umbrella simulation index 1..S, col 2:(2+dim-1) = biased simulation umbrella centers / (arbitrary units), col (2+dim):(2+2*dim-1) = harmonic restraining potential force constant / kJ/mol.(arbitrary units)^2 
% histBinEdgesFile          - [str] path to text file containing dim rows specifying edges of the rectilinear histogram bins in each dimension used to construct dim-dimensional histograms held in histDir/hist_i.txt 
%							  -> histBinEdgesFile contains k=1..dim lines each holding a row vector specifying the rectilinear histogram bin edges in dimension k 
%                             -> for M_k bins in dimension k, there are (M_k+1) edges 
%                             -> bins need not be regularly spaced
% histDir                   - [str] path to directory holding i=1..S dim-dimensional histograms in files hist_i.txt compiled from S biased trajectories over dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> hist_i.txt comprises a row vector containing product_k=1..dim M_k = (M_1*M_2*...*M_dim) values recording histogram counts in each bin of the rectilinear histogram 
%                             -> values recorded in row major order (last index changes fastest) 
% tol_WHAM                  - [float] tolerance on maximum change in any element of p(psi) in self-consistent WHAM solution / - 
% maxIter_WHAM              - [int] maximum # steps allowed to converge to MAP probability estimate by direct iteration of WHAM equations 
% steps_MH                  - [int] # steps in Metropolis-Hastings sampling of Bayes posterior in p(psi) 
% saveMod_MH                - [int] Metropolis-Hastings save modulus 
% printMod_MH               - [int] Metropolis-Hastings print to screen modulus 
% maxDpStep_MH              - [float] maximum size of uniformly distributed step in p(psi) in randomly selected bin 
%                             -> tune to get acceptance probabilities of ~5-20% is a good rule of thumb 
% seed_MH					- [int] seed to initialize random number generator used to perform Metropolis-Hastings sampling of Bayes posterior
% prior                     - [str] type of prior to employ in Bayesian estimate of posterior probability distribution given umbrella sampling histograms
%                             -> prior = {'none','Dirichlet','Gaussian'} 
%                             -> N.B. WHAM equations are solved and prior applied to only the non-zero histogram bins (i.e., in case of Dirichlet pseudo-counts are only added to non-zero count histogram bins) 
% alpha                     - [float] hyperparameter of prior distribution
%                             -> prior = 'Dirichlet' : concentration parmeter for (symmetric) Dirichlet prior, equivalent to adding pseudo-counts of (alpha-1) to each histogram bin; alpha = 1 => uniform prior; alpha > 1 => favors dense + even posteriors, 0 < alpha < 1 => sparse + concentrated posteriors 
%                                        'Gaussian'  : scale parameter for Gaussian penalization of probabilities, exp(-alpha*p^2) 

% OUTPUTS
%
% hist_binCenters.txt       - [dim x M_k float] text file containing dim rows specifying centers of the rectilinear histogram bins in each dimension used to construct dim-dimensional histograms held in histDir/hist_i.txt 
%                             -> dimension k contains M_k bin centers 
%                             -> bins need not be regularly spaced
% hist_binWidths.txt        - [dim x M_k float] text file containing dim rows specifying widths of the rectilinear histogram bins in each dimension used to construct dim-dimensional histograms held in histDir/hist_i.txt 
%                             -> dimension k contains M_k bin widths 
%                             -> bins need not be regularly spaced
% p_MAP.txt                 - [1 x M float] text file containing MAP estimate of unbiased probability distribution p_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to total probability residing within bin 
%                             -> values recorded in row major order (last index changes fastest) 
% pdf_MAP.txt               - [1 x M float] text file containing MAP estimate of unbiased probability density function pdf_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to average probability density over the bin 
%                             -> values recorded in row major order (last index changes fastest) 
% betaF_MAP.txt             - [1 x M float] text file containing MAP estimate of unbiased free energy surface beta*F_l = -ln(p(psi)/binVolume) + const. over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile to within an arbitrary additive constant 
%                             -> specified only up to an additive constant defined by shifting landscape to have zero mean; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
%                             -> values recorded in row major order (last index changes fastest) 
% f_MAP.txt                 - [1 x S float] text file containing MAP estimates of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
% logL_MAP.txt              - [float] text file containing log of Bayes posterior up to an additive constant of the MAP estimate 
% p_MH.txt                  - [nSamples_MH x M float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased probability distribution p_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to total probability residing within bin, NOT probability density
%                             -> values recorded in row major order (last index changes fastest) 
% pdf_MH.txt                - [nSamples_MH x M float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased probability density function pdf_l over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> values correspond to average probability density over the bin 
%                             -> values recorded in row major order (last index changes fastest) 
% betaF_MH.txt              - [nSamples_MH x M float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased free energy surface beta*F_l = -ln(p(psi)/binVolume) + const. over l=1..M bins of dim-dimensional rectilinear histogram grid specified in histBinEdgesFile to within an arbitrary additive constant 
%                             -> specified only up to an additive constant defined by shifting landscape to have zero mean; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
%                             -> values recorded in row major order (last index changes fastest) 
% f_MH.txt                  - [nSamples_MH x S float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
% logL_MH.txt               - [nSamples_MH x 1 float] text file containing log of Bayes posterior up to an additive constant over the nSamples_MH Metropolis-Hastings samples from the posterior 
% step_MH.txt               - [nSamples_MH x 1 int] text file containing Metropolis-Hastings step associated with each of the nSamples_MH Metropolis-Hastings samples from the posterior 

"""

## imports
import os, re, sys, time
import random, math
from scipy.integrate import simps
import numpy as np
import numpy.matlib
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components

## classes

## methods
def mmfit(x, vmax, km):
	return vmax * x / (km+x)
# usage
def _usage():
	print "USAGE: %s dim periodicity T harmonicBiasesFile histBinEdgesFile histDir tol_WHAM maxIter_WHAM steps_MH saveMod_MH printMod_MH maxDpStep_MH seed_MH prior alpha" % sys.argv[0]
	print "       dim                       - [int] dimensionality of umbrella sampling data in psi(1:dim) = number of coordinates in which umbrella sampling was conducted "
	print "       periodicity               - [1 x dim bool] periodicity in each dimension across range of histogram bins "
	print "                                   -> periodicity(i) == 0 => no periodicity in dimension i; periodicity(i) == 1 => periodicity in dimension i with period defined by range of histogram bins "
	print "       T                         - [float] temperature in Kelvin at which all umbrella sampling simulations were conducted "
	print "       harmonicBiasesFile        - [str] path to text file containing (1 + 2*dim) columns and S rows listing location and strength of biasing potentials in each of the S umbrella simulations "
	print "                                   -> col 1 = umbrella simulation index 1..S, col 2:(2+dim-1) = biased simulation umbrella centers / (arbitrary units), col (2+dim):(2+2*dim-1) = harmonic restraining potential force constant / kJ/mol.(arbitrary units)^2 "
	print "       histBinEdgesFile          - [str] path to text file containing dim rows specifying edges of the rectilinear histogram bins in each dimension used to construct dim-dimensional histograms held in histDir/hist_i.txt "
	print "                                   -> histBinEdgesFile contains k=1..dim lines each holding a row vector specifying the rectilinear histogram bin edges in dimension k "
	print "                                   -> for M_k bins in dimension k, there are (M_k+1) edges "
	print "                                   -> bins need not be regularly spaced "
	print "       histDir                   - [str] path to directory holding i=1..S dim-dimensional histograms in files hist_i.txt compiled from S biased trajectories over dim-dimensional rectilinear histogram grid specified in histBinEdgesFile "
	print "                                   -> hist_i.txt comprises a row vector containing product_k=1..dim M_k = (M_1*M_2*...*M_dim) values recording histogram counts in each bin of the rectilinear histogram "
	print "                                   -> values recorded in row major order (last index changes fastest) "
	print "       tol_WHAM                  - [float] tolerance on maximum change in any element of p(psi) in self-consistent WHAM solution / - "
	print "       maxIter_WHAM              - [int] maximum # steps allowed to converge to MAP probability estimate by direct iteration of WHAM equations "
	print "       steps_MH                  - [int] # steps in Metropolis-Hastings sampling of Bayes posterior in p(psi) "
	print "       saveMod_MH                - [int] Metropolis-Hastings save modulus "
	print "       printMod_MH               - [int] Metropolis-Hastings print to screen modulus "
	print "       maxDpStep_MH              - [float] maximum size of uniformly distributed step in p(psi) in randomly selected bin "
	print "                                   -> tune to get acceptance probabilities of ~5-20% is a good rule of thumb "
	print "       seed_MH					- [int] seed to initialize random number generator used to perform Metropolis-Hastings sampling of Bayes posterior "
	print "       prior                     - [str] type of prior to employ in Bayesian estimate of posterior probability distribution given umbrella sampling histograms "
	print "                                   -> prior = {'none','Dirichlet','Gaussian'} "
	print "                                   -> N.B. WHAM equations are solved and prior applied to only the non-zero histogram bins (i.e., in case of Dirichlet pseudo-counts are only added to non-zero count histogram bins) "
	print "       alpha                     - [float] hyperparameter of prior distribution "
	print "                                   -> prior = 'Dirichlet' : concentration parmeter for (symmetric) Dirichlet prior, equivalent to adding pseudo-counts of (alpha-1) to each histogram bin; alpha = 1 => uniform prior; alpha > 1 => favors dense + even posteriors, 0 < alpha < 1 => sparse + concentrated posteriors "
	print "                                              'Gaussian'  : scale parameter for Gaussian penalization of probabilities, exp(-alpha*p^2) "	
	
# def unique(a):
#     """ return the list with duplicate elements removed """
#     return list(set(a))

# def intersect(a, b):
#     """ return the intersection of two lists """
#     return list(set(a) & set(b))

# def union(a, b):
#     """ return the union of two lists """
#     return list(set(a) | set(b))

def ind2sub_RMO(sz,ind):
	
	if (ind < 0) | (ind > (np.prod(sz)-1)):
		print("\nERROR - Variable ind = %d < 0 or ind = %d > # elements in tensor of size sz = %d in ind2sub_RMO" % (ind,ind,np.prod(sz)))
		sys.exit(-1)
	
	sub = np.zeros(sz.shape[0], dtype=np.uint64)
	P = np.cumprod(sz[1:])[::-1]
	for ii in range(0,len(sz)-1):
		#P = np.prod(sz[ii+1:])
		sub_ii = math.floor(float(ind)/float(P[ii]))
		sub[ii] = sub_ii
		ind = ind - sub_ii*P[ii]
		if ind == 0: #Since sub is already set to zero, no point in continuing past this, since subsequent sets will always result in sub[ii] = 0.
			break
	sub[-1] = ind
	
	return sub

def sub2ind_RMO(sz,sub):
	
	if sz.shape != sub.shape:
		print("\nERROR - Variables sub and sz in sub2ind_RMO are not commensurate")
		sys.exit(-1)
	
	for ii in range(0,len(sz)-1):
		if ( sub[ii] < 0 ) | ( sub[ii] >= sz[ii] ):
			print("\nERROR - sub[%d] = %d < 0 or sub[%d] = %d >= sz[%d] = %d in sub2ind_RMO"  % (ii,sub[ii],ii,sub[ii],ii,sz[ii]))
			sys.exit(-1)
	
	#ind=0
	ind = np.sum(sub[:-1] * np.cumprod(sz[1:])[::-1]) + sub[-1]
	# for ii in range(0,len(sz)-1):
	# 	ind = ind + sub[ii]*np.prod(sz[ii+1:])
	# ind = ind + sub[-1]

	return int(ind)

def bayeswham(histograms, edges, biasforces, biascenters, kt=0.59616108, periodicity=False, tol_WHAM=1e-8, maxIter_WHAM=1e6, gibbsiter=10000, gibbssamples=100, seed_MH=-1):
	beta = 1.0/kt
	if not isinstance(edges[0], (list, tuple, np.ndarray)):
		print("\nERROR - edges list is not deep enough! edges[0] is not a list, tuple, or numpy array!")
		print("\nDid you potentially mean to pass [edges]?")
		raise ValueError
	dim = len(edges)
	if type(periodicity) == type(True):
		periodicity = np.ones(dim) * periodicity
	if seed_MH == -1:
		seed_MH = int(time.time())
	# - printing args to screen
	print("")
	print("dim = %d" % (dim))
	print("periodicity = %s" % (periodicity))
	print("beta = %e" % (beta))
	print("tol_WHAM = %e" % (tol_WHAM))
	print("maxIter_WHAM = %d" % (maxIter_WHAM))
	print("seed_MH = %d" % (seed_MH))
	print("")
	if biasforces.shape != (len(histograms), len(edges)):
		print("\nERROR - biasforces shape does not match the shape expected from the number of histograms and the number of edge arrays. Got shape %s, expected shape %s" % (str(biasforces.shape), str((len(histograms), len(edges)))))
		raise ValueError
	if biascenters.shape != (len(histograms), len(edges)):
		print("\nERROR - biascenters shape does not match the shape expected from the number of histograms and the number of edge arrays. Got shape %s, expected shape %s" % (str(biascenters.shape), str((len(histograms), len(edges)))))
		raise ValueError
	for i, edgearr in enumerate(edges):
		if len(edgearr) != histograms[0].shape[i]+1:
			print("\nERROR - dimension %d has the wrong number of bins! I found %d but expected %d based on the given histograms." % (i+1, len(edgearr), histograms[0].shape[i]+1))
			raise ValueError
		if not np.all(np.diff(edgearr) > 0):
			print("\nERROR - dimension %d has non-monotonic bins" % (i+1))
			print("\nThis is the edge array:\n%s" % str(edgearr))
			raise ValueError
	for i, hist in enumerate(histograms[1:]):
		if hist.shape != histograms[0].shape:
			print("\nERROR - histogram %d does not match the shape of the first histogram. Found shape %s, expected shape %s" % (i+2, str(hist.shape), str(histograms[0].shape)))
			raise ValueError
	# loading data
	print("Creating datastructures...")
	S = len(histograms)
	umbC = biascenters
	umbF = biasforces
	binE = edges
	M_k = np.array([len(i) - 1 for i in binE], dtype=np.uint64)
	M = np.prod(M_k)
	binC = []
	binW = []
	for d in range(0,dim):
		binC.append(0.5 * (binE[d][:-1] + binE[d][1:]))
		binW.append(binE[d][1:] - binE[d][:-1])
	# - computing period in each periodic dimension as histogram bin range 
	period = np.zeros(dim)
	for d in range(0,dim):
		if periodicity[d] == 0:
			period[d] = float('nan')
		else:
			period[d] = binE[d][-1] - binE[d][0]
	# loading histograms compiled over the S umbrella simulations recorded as row vectors in row major order (last index changes fastest) 
	n_il = []
	for i in range(0,S):
		n_il.append(histograms[i].flatten(order='C'))
	n_il = np.vstack(n_il)
	# precomputing aggregated statistics
	N_i = np.sum(n_il,axis=1)         # total counts in simulation i
	M_l = np.sum(n_il,axis=0)         # total counts in bin l
	print("DONE!\n\n")
	print("Checking connectivity of aggregated biased histogram...")
	# checking M_l for gaps 
	# -> gaps in the aggregated M_l histogram (i.e., empty bins between populated bins) can cause numerical stability problems for the WHAM equations 
	#    - conceptually, the presence of gaps indicates that there is not mutual (at least pairwise) overlap between all biased histograms, meaning that the estimate of the unbiased distribution may be disjoint since the biased distributions cannot be stitched together into a continuous probability distribution 
	#    - mathematically, p_l becomes disjoint and cannot be consistently normalized across the full domain resulting in multiple disjoint distributions that leads to numerical instability in the WHAM equations 
	# -> contiguity of the histogram (i.e., the absence of empty bins between populated bins) is a sufficient, but not necessary, condition to assure stability of the WHAM equations since this guarantees mutual overlap between all of the biased histograms and a non-disjoint estimate of the unbiased probability distribution (sufficiently small gaps are tolerable provided that the probability distribution does not become disjoint within machine precision) 
	# - computing adjacency matrix representing the graph specifying contiguity of populated histogram bins 
	adjacency = np.zeros((M,M))
	for l in range(0,M):
		if M_l[l] > 0:
			sub = ind2sub_RMO(M_k,l)
			#print sub
			for d in range(0,dim):
				for q in [-1,+1]:
					
					sub_d_plus_q = sub[d] + q
					if sub_d_plus_q == M_k[d]:
						if periodicity[d] == 0:
							continue
						else:
							sub_d_plus_q = 0			# wrapping through periodic boundary
					elif sub_d_plus_q == -1:
						if periodicity[d] == 0:
							continue
						else:
							sub_d_plus_q = M_k[d]-1		# wrapping through periodic boundary
					sub_d_plus_q = int(sub_d_plus_q)
					
					sub_neigh = np.array(sub)
					sub_neigh[d] = sub_d_plus_q
					
					ind_neigh = sub2ind_RMO(M_k,sub_neigh)
					if M_l[ind_neigh] > 0:
						adjacency[l,ind_neigh] = 1
			
	# - applying Tarjan's algorithm to identify contiguous regions of non-zero histogram bins within the adjacency matrix 
	maskNZ = (M_l>0)
	adjacency_NZ = adjacency[:,maskNZ]
	adjacency_NZ = np.transpose(np.transpose(adjacency_NZ)[:,maskNZ])
	adjacency_NZ_SPARSE = csr_matrix(adjacency_NZ)
	[nConComp,conComp_labels] = connected_components(adjacency_NZ_SPARSE, directed=False, connection='weak', return_labels=True)
	if nConComp > 1:
		print("\nWARNING - aggregated biased histogram is disconnected, containing %d non-contiguous connected components\n" % (nConComp))
		print("        - we might anticipate WHAM convergence difficulties\n")
		print("        - consider making the aggregated histogram contiguous by increasing the bin size or conducting additional biased runs to patch the gaps\n")
		time.sleep(1)
	print("DONE!\n\n")

	# precomputing biasing potentials for each simulation i in each bin l 
	print("Computing biasing potentials for each simulation in each bin...")
	c_il = np.zeros((S,M))		# S-by-M matrix containing biases due to artificial harmonic potential for simulation i in bin l
	for i in range(0,S):
		for l in range(0,M):
			sub = ind2sub_RMO(M_k,l)
			expArg = 0
			for d in range(0,dim):
				if periodicity[d] != 0:
					delta = min( abs(binC[d][sub[d]]-umbC[i,d]), abs(binC[d][sub[d]]-umbC[i,d]+period[d]), abs(binC[d][sub[d]]-umbC[i,d]-period[d]) )
				else:
					delta = abs(binC[d][sub[d]]-umbC[i,d])
				expArg = expArg + 0.5*umbF[i,d]*math.pow(delta,2)
			c_il[i,l] = expArg
		print("\tProcessing of simulation %d of %d complete" % (i+1,S))
	c_il = np.exp(-beta * c_il)
	print("DONE!\n\n")

	# MAP estimate of unbiased probability distribution over the umbrella histogram bins p_l by direct iteration of the WHAM equations
	# -> analogy of WHAM equations under Bayesian prior derived using method of Lagrangian multipliers 
	# -> Ref: C. Bartels "Analyzing biased Monte Carlo and molecular dynamics simulations" Chemical Physics Letters 331 446-454 (2000)
	# - masking out all zero-count bins not visited by any simulation for computational efficiency 
	#   -> optimal probability estimate for unvisited bins with M_l=0 is p_l=0, so can eliminate these bins from consideration 
	#   -> Ref: C. Bartels "Analyzing biased Monte Carlo and molecular dynamics simulations" Chemical Physics Letters 331 446-454 (2000)
	maskNZ = (M_l>0)
	M_l_NZ = np.array(M_l[maskNZ])
	c_il_NZ = np.array(c_il[:,maskNZ])
	M_NZ = M_l_NZ.size
	# - initial guess for p_l and f_i
	p_l_NZ = np.ones(M_NZ)
	p_l_NZ = np.divide( p_l_NZ, np.sum(p_l_NZ) )
	f_i = np.divide( 1, np.dot(c_il_NZ,p_l_NZ) )
	# - direct iteration of WHAM equations to self-consistency at tolerance = tol 
	dp_max = float('Inf')
	dlogf_max = float('Inf')
	iter = 1
	print("Computing MAP estimate of probability distribution by direct iteration of the WHAM equations...")
	while (dlogf_max > tol_WHAM):
		
		p_l_NZ_OLD = np.array(p_l_NZ)
		f_i_OLD = np.array(f_i)
		
		p_l_NZ = np.divide( M_l_NZ, np.dot( np.multiply(N_i,f_i), c_il_NZ ) )
		
		p_l_NZ = np.divide( p_l_NZ, np.sum(p_l_NZ) )
		f_i = np.divide( 1, np.dot(c_il_NZ,p_l_NZ) )
		
		dp_max = max( abs(p_l_NZ - p_l_NZ_OLD) )
		if not np.isfinite(dp_max):
			print("\nERROR - WHAM equation convergence failed")
			sys.exit(-1)
		
		log_f_i = np.log(f_i) #np.array([math.log(x) for x in f_i])
		log_f_i_OLD = np.log(f_i_OLD) #np.array([math.log(x) for x in f_i_OLD])
		dlogf_max = max( abs( log_f_i - log_f_i_OLD ) )
		
		print("\tIteration %d, max(dp) = %15.5e" % (iter,dlogf_max))
		
		iter = iter + 1
		if iter > maxIter_WHAM:
			print("\nERROR - WHAM iteration did not converge within maxIter_WHAM = %d" % (maxIter_WHAM))
			raise RuntimeError
			
	print("\tWHAM equations converged to within tolerance of %15.5e" % (tol_WHAM))
	print("DONE!\n\n")
	# - unmasking p_l_NZ into p_l_MAP by reconstituting empty bins l with M_l=0 with optimal values p_l=0 
	p_l_MAP = np.zeros(M)
	p_l_MAP[maskNZ] = p_l_NZ
	f_i_MAP = np.array(f_i)
	# - converting unbiased probability distribution estimate p_l into probability density function pdf_l and free energy estimate betaF_l 
	#   -> mean zeroing betaF; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
	pdf_l_MAP = np.zeros(p_l_MAP.size)
	betaF_l_MAP = np.ones(p_l_MAP.size)*float('nan')
	binvols = np.ones(M, dtype=np.float)
	for l in range(0,M):
		sub = ind2sub_RMO(M_k,l)
		binVol = 1
		for d in range(0,dim):
			binVol = binVol*binW[d][sub[d]]
		binvols[l] = binVol
	#if p_l_MAP[l] > 0:
	pdf_l_MAP = p_l_MAP * (p_l_MAP >0) /binvols
	betaF_l_MAP = -np.log(p_l_MAP/binvols) * (p_l_MAP >0)
	betaF_l_MAP = betaF_l_MAP - np.mean(betaF_l_MAP[np.isfinite(betaF_l_MAP)])
	output = dict()
	output['hist_binCenters'] = binC
	output['hist_binWidths'] = binW
	output['p_MAP'] = p_l_MAP.reshape(histograms[0].shape)
	output['pdf_MAP'] = pdf_l_MAP.reshape(histograms[0].shape)
	output['betaF_MAP'] = betaF_l_MAP.reshape(histograms[0].shape)
	output['f_MAP'] = f_i_MAP
	# - writing to file log of Bayes posterior up to an additive constant for the MAP estimate 
	maskNZ = (M_l>0)
	M_l_NZ = np.array(M_l[maskNZ])
	p_l_MAP_NZ = np.array(p_l_MAP[maskNZ])
	M_NZ = M_l_NZ.size
	logL_MAP = np.dot( N_i, np.log(f_i_MAP) ) + np.dot( M_l_NZ, np.log(p_l_MAP_NZ))
	#**JVV** This is the section that changes! Draw samples from the appropriate gamma distributions. It is much faster than MH sampling.
	np.random.seed(seed_MH)
	output['logL_MAP'] = logL_MAP
	flist = []
	plist = []
	for i in range(gibbssamples):
		print("Gibbs sample: %d" % (i+1))
		f=output['f_MAP']
		for j in range(gibbsiter):
			p = np.random.gamma(M_l , 1.0/ np.dot( f * N_i , c_il ))
			p /= np.sum(p)
			f = np.random.gamma(N_i , 1.0/(N_i * np.dot(c_il,p)))
		flist.append(f)
		bf = -np.log(p/binvols) * (p>0)
		bf -= np.mean(bf[np.isfinite(bf)])
		plist.append(bf.reshape(histograms[0].shape))
	output['f_samples'] = np.vstack(flist)
	output['betaF_samples'] = np.vstack(plist)
	output['beta'] = beta
	return output


def createhistograms(trajdata, binedges):
	histograms = []
	for i in range(len(trajdata)):
		hist, edges = np.histogramdd(trajdata[i], bins=binedges)
		histograms.append(hist)
	return histograms
histograms = []
ndim=2
edge1 = np.linspace(-180, 180, 37)
edge2 = np.hstack([np.linspace(-180,-110, 5) , edge1[8:]])
for i in range(36):
	data = np.loadtxt("diala_phi_psi_EXAMPLE/traj/traj_%d.txt"  % (i+1))
	hist, edges = np.histogramdd(data, bins=[edge1, edge2])
	histograms.append(hist)
centernforces = np.loadtxt("diala_phi_psi_EXAMPLE/bias/harmonic_biases.txt")
umbC = centernforces[:, 1:1+ndim]
umbF = centernforces[:, 1+ndim:]
print(bayeswham(histograms, edges, umbF, umbC, periodicity=True, seed_MH=200184, kt=2.47770904))
